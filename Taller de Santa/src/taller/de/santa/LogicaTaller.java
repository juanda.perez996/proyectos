/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller.de.santa;

import javax.swing.JOptionPane;

/**
 *
 * @author Juan Daniel
 */
public class LogicaTaller {

    private Formulario[] usuarios;
    private FormularioNiños[] ninnos;
   

    public LogicaTaller() {
        usuarios = new Formulario[100];
        ninnos = new FormularioNiños[100];
    }

    /**
     * verifica si el que esta ingresando es Santa o es un Duende
     *
     * @return 0 si es santa y 1 si es un duende
     */
    public int verificacion(int log) {
        int x = 0;
        switch (log) {
            case 0:
                x = 0;
                break;
            case 1:
                x = 1;
                break;
            default:
                x = 3;
                break;
        }
        return x;
    }

    /**
     * registra a un usuario nuevo en el sistema
     *
     * @param usuario los datos del usuario nuevo
     *
     */
    public void Registrar(Formulario usuario) {
        for (int i = 0; i < usuarios.length; i++) {
            if (usuarios[i] == null) {
                usuarios[i] = usuario;
                break;
            }
        }
    }

    /**
     * registra los datos de un niño nuevo
     *
     * @param ninno los datos del nuevo niño
     *
     */
    public void RegistrarNiños(FormularioNiños ninno) {
        for (int i = 0; i < ninnos.length; i++) {
            if (ninnos[i] == null) {
                ninnos[i] = ninno;
                break;
            }
        }
    }
    


    /**
     * busca a un usuario en el arreglo de usuarios existentes
     *
     * @param id numero de cedula del usuario
     * @param pass contraseña
     * @return 0 si es true, 1 si es false
     */
    public int buscarUsuario(int id, String pass) {
        int type = 2;
        for (Formulario usuario : usuarios) {
            if (usuario != null && usuario.getId() == (id)
                    && usuario.getPass().equals(pass)) {
                if (usuario.getType() == 0) {
                    type = 0;
                    break;
                } else if (usuario.getType() == 1) {
                    type = 1;
                }
                break;
            }
        }
        return type;
    }

    /**
     * verifica que el nombre exista en el arreglo de usuarios e imprime los datos 
     *
     * 
     * @return id y pass del usuario y si es duende o santa 
     */
    public String verExist() {
        String dats = "";
        for (Formulario usuario : usuarios) {

            if (usuario != null) {
                dats += usuario.getId() + " " + usuario.getPass() + " " + usuario.getType() + "\n";

            }
        }
        return dats;
    }
    /**
     * verifica que el nombre exista en el arreglo de usuarios e imprime los datos
     * @return  id,nombre,fecha de nacimiento,genero y ciudad
     */
    public String verExistNiño() {
        String dats = "";
        for (FormularioNiños ninno : ninnos) {
            if (ninno != null) {
                dats += ninno.getId() + "-" + ninno.getName()+ "-" + ninno.getDiaNac()+"/"+ninno.getMesNac()+"/"+ninno.getAñoNac()+"-"+ninno.getGenero()+"-"+ninno.isCiudad()+ "\n";

            }
        }
        return dats;
    }

    /**
     * muestra el menu de santa
     *
     * @param opt
     */
    public void menuSanta(int opt) {
        switch (opt) {

        }
    }

    /**
     * muestra el menu de los Duendes
     *
     * @param opt
     */
    public void menuDuende(int opt) {
        switch (opt) {

        }
    }
    
    /**
     * imprime la collida de los pedidos donde llevara las especificaciones de cada regalo
     * @param nombre nombre del niño y otros datos de interes
     * @return 
     */
    public String imprimirColilla(String nombre) {
        String coli = "";
        for (FormularioNiños ninno : ninnos) {

            //preguntar si la colilla esta y imprimir los datos de la colilla 
        }//return para quitar error
        return null;

    }
    /**
     * realiza los pedidos de cada niño si las cartas estan aprobadas y hay insumos 
     * @return pedidos
     */
    public boolean RealizarPedido() {
        

        return false;

    }
    /**
     * borra los pedidos listos del sistema 
     * @return eliminacion de pedido
     */
    public boolean FinalizarPedido() {
        return false;

    }
    
    
}
