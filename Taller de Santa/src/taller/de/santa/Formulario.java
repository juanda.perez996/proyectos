/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller.de.santa;

/**
 *
 * @author Juan Daniel
 */
public class Formulario {

    private int id;
    private String name;
    private int diaNac;
    private int mesNac;
    private int añoNac;
    private String email;
    private int type;
    private String pass;

    public Formulario(int id, String name, int diaNac, int mesNac,
            int añoNac, String email, int type, String pass) {
        this.id = id;
        this.name = name;
        this.diaNac = diaNac;
        this.mesNac = mesNac;
        this.añoNac = añoNac;
        this.email = email;
        this.type = type;
        this.pass = pass;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAñoNac(int añoNac) {
        this.añoNac = añoNac;
    }

    public void setMesNac(int mesNac) {
        this.mesNac = mesNac;
    }

    public void setDiaNac(int diaNac) {
        this.diaNac = diaNac;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPass() {
        return pass;
    }

    public String getEmail() {
        return email;
    }

    public int getAñoNac() {
        return añoNac;
    }

    public int getMesNac() {
        return mesNac;
    }

    public int getDiaNac() {
        return diaNac;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getType() {
        return type;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setType(int type) {
        this.type = type;
    }

}
