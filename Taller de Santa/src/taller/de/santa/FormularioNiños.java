/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller.de.santa;

/**
 *
 * @author aleDH
 */
public class FormularioNiños {
    private int idN;
    private String name;
    private int diaNac;
    private int mesNac;
    private int añoNac;
    private String genero;
    private String ciudad;

    public FormularioNiños(int idN, String name, int diaNac, int mesNac, int añoNac, String genero, String ciudad) {
        this.idN = idN;
        this.name = name;
        this.diaNac = diaNac;
        this.mesNac = mesNac;
        this.añoNac = añoNac;
        this.genero = genero;
        this.ciudad = ciudad;
    }

    public int getId() {
        return idN;
    }

    public String getName() {
        return name;
    }

    public int getDiaNac() {
        return diaNac;
    }

    public int getMesNac() {
        return mesNac;
    }

    public int getAñoNac() {
        return añoNac;
    }

    public String getGenero() {
        return genero;
    }

    public String isCiudad() {
        return ciudad;
    }

    public void setId(int idN) {
        this.idN = idN;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDiaNac(int diaNac) {
        this.diaNac = diaNac;
    }

    public void setMesNac(int mesNac) {
        this.mesNac = mesNac;
    }

    public void setAñoNac(int añoNac) {
        this.añoNac = añoNac;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
    
    
    
    
}
