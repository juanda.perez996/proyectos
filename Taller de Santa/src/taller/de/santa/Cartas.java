/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package taller.de.santa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Juan Daniel
 */
public class Cartas {
    /**
     * verifica si es un archivo o un directorio
     * @param dir
     * @return directorio o archivo
     */
    public String leerCarpeta(File dir){
        try{
            File[] files = dir.listFiles();
            for (File file : files)
                if (file.isDirectory()){
                    System.out.println("Directorio: " + file.getCanonicalFile());
                }else{
                    System.out.println("Archivo:" + file.getCanonicalPath());
                }
        }catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }
    /**
     * lee los archivos de texto de un directorio con una ruta 
     * @param direction
     * @return informacion 
     */
    public String leerTxt(String direction){
        String texto="";
        try {
            BufferedReader bf = new BufferedReader(new FileReader(direction));
            String tempo ="";
            String bfReader;
            while ((bfReader = bf.readLine())!=null){
                tempo += bfReader;
            }
            texto = tempo;
        }catch(Exception e){
            texto = "No se encontraron datos en el archivo";
        }
        return texto;
    
    }
    

    
    
    
}
