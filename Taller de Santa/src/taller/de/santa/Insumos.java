package taller.de.santa;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Juan Daniel
 */
public class Insumos {

    private int madera;
    private int metal;
    private int pintura;
    private int clavos;
    private int tornillos;

    public Insumos() {
        this.madera = 10;
        this.metal = 10;
        this.pintura = 10;
        this.clavos = 100;
        this.tornillos = 100;
    }

    public int getMadera() {
        return madera;
    }

    public int getMetal() {
        return metal;
    }

    public int getPintura() {
        return pintura;
    }

    public int getClavos() {
        return clavos;
    }

    public int getTornillos() {
        return tornillos;
    }

    public void setMadera(int madera) {
        this.madera = madera;
    }

    public void setMetal(int metal) {
        this.metal = metal;
    }

    public void setPintura(int pintura) {
        this.pintura = pintura;
    }

    public void setClavos(int clavos) {
        this.clavos = clavos;
    }

    public void setTornillos(int tornillos) {
        this.tornillos = tornillos;
    }

}
